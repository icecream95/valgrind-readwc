# valgrind-readwc

A Valgrind tool for reporting reads from write-combine memory.

Currently this reports on all read instructions from BOs coming from
graphics drivers that use device nodes in `/dev/dri`, but adapting it
to also work for other sources of write-combine memory or tracing
writes as well as reads should be trivial.

## Compilation

From the Valgrind source directory:

```
$ git clone https://gitlab.com/icecream95/valgrind-readwc readwc
$ patch -p1 <readwc/valgrind.patch
$ ./autogen.sh
$ ./configure --prefix=`pwd`/inst
$ make -j`nproc`
$ make install 
```

Example usage:

```
$ ~/valgrind-3.18.1/inst/bin/valgrind --tool=readwc glmark2-es2
==28579== ReadWC, warn on reads from WC memory
==28579== Copyright (C) 2021, and GNU GPL'd, by Icecream95.
==28579== Using Valgrind-3.18.1 and LibVEX; rerun with -h for copyright info
==28579== Command: glmark2-es2
==28579== 
=======================================================
    glmark2 2021.12
=======================================================
    OpenGL Information
    GL_VENDOR:     Panfrost
    GL_RENDERER:   Mali-G72 (Panfrost)
    GL_VERSION:    OpenGL ES 3.1 Mesa 21.3.0-devel (git-580fd7587a)
=======================================================
[build] use-vbo=false:==28579==    at 0x5CCA910: panfrost_emit_const_buf.lto_priv.2 (in /tmp/panfrost/lib/dri/panfrost_dri.so)
==28579==    by 0x5CCE0B3: panfrost_update_state_vs.lto_priv.0 (in /tmp/panfrost/lib/dri/panfrost_dri.so)
==28579==    by 0x5CD3CFB: panfrost_direct_draw (in /tmp/panfrost/lib/dri/panfrost_dri.so)
==28579==    by 0x5CD464B: panfrost_draw_vbo (in /tmp/panfrost/lib/dri/panfrost_dri.so)
==28579==    by 0x5F440E3: u_vbuf_draw_vbo.constprop.0 (in /tmp/panfrost/lib/dri/panfrost_dri.so)
==28579==    by 0x5BCFA5F: cso_multi_draw (in /tmp/panfrost/lib/dri/panfrost_dri.so)
==28579==    by 0x576E967: _mesa_DrawArrays (in /tmp/panfrost/lib/dri/panfrost_dri.so)
==28579==    by 0x15B02F: Mesh::render_array() (mesh.cpp:555)
==28579==    by 0x1223EF: SceneBuild::draw() (scene-build.cpp:245)
==28579==    by 0x11D35F: MainLoop::draw() (main-loop.cpp:133)
==28579==    by 0x120247: MainLoop::step() (main-loop.cpp:108)
==28579==    by 0x111BD7: main (main.cpp:123)
==28579== 
 FPS: 294 FrameTime: 3.401 ms
=======================================================
                                  glmark2 Score: 294 
=======================================================
==28579== 
==28579== For lists of detected and suppressed errors, rerun with: -s
==28579== ERROR SUMMARY: 3138 errors from 1 contexts (suppressed: 0 from 0)
```
