
/*--------------------------------------------------------------------*/
/*--- ReadWC: Check for WC reads from Panfrost BOs.      wc_main.c ---*/
/*--------------------------------------------------------------------*/

/*
   This file is part of ReadWC, a Valgrind tool.

   Copyright (C) 2021 Icecream95
      ixn@disroot.org

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, see <http://www.gnu.org/licenses/>.

   The GNU General Public License is contained in the file COPYING.
*/

#include "pub_tool_basics.h"
#include "pub_tool_tooliface.h"
#include "pub_tool_threadstate.h"
#include "pub_tool_libcassert.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_libcproc.h"
#include "pub_tool_debuginfo.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_options.h"
#include "pub_tool_stacktrace.h"
#include "pub_tool_machine.h"     // VG_(fnptr_to_fnentry)

#include <stdint.h>
#include <syscall.h>

typedef struct _WC_Error {
   Addr addr;
} WC_Error;

typedef
   IRExpr 
   IRAtom;

static void wc_post_clo_init (void)
{
}

/* BOs will probably end up mapped into the first 8 GB of address
 * space (Valgrind adjusts mmap addresses). */
#define N_PRIMARY_BITS 33

#define P_PAGE_BITS 12 
#define P_PAGE_SIZE (1ULL << P_PAGE_BITS) 
#define PRIMARY_SIZE (1ULL << (N_PRIMARY_BITS - P_PAGE_BITS))
#define MAX_PRIMARY_ADDR ((1ULL << N_PRIMARY_BITS) - 1)

static uint8_t primary[PRIMARY_SIZE];

static VG_REGPARM(2) void trace_load ( Addr iaddr, Addr addr )
{
   if (addr > MAX_PRIMARY_ADDR) {
      return;
   }

   uint8_t *p = primary + (addr >> P_PAGE_BITS);
   if (*p == 1) {
      VG_(maybe_record_error)( VG_(get_running_tid)(), 0,
                               0, NULL, NULL );

//      VG_(printf)(" L %lx,%08lx\n", iaddr, addr);
//      VG_(get_and_pp_StackTrace)(VG_(gettid)(), VG_(clo_backtrace_size));
//      *p = 2;
   }
}

static VG_REGPARM(1) void trace_load_no_iaddr ( Addr addr )
{
   trace_load(0, addr);
}

static
void addEvent_Dr_guarded ( IRSB* sb, IRAtom* iaddr, IRAtom* daddr, IRAtom* guard )
{
   IRDirty* di;

   if (iaddr) {
      IRExpr** argv = mkIRExprVec_2( iaddr, daddr );
      di   = unsafeIRDirty_0_N( /*regparms*/2,
                                "trace_load", VG_(fnptr_to_fnentry)( trace_load ),
                                argv );
   } else {
      IRExpr** argv = mkIRExprVec_1( daddr );
      di   = unsafeIRDirty_0_N( /*regparms*/1,
                                "trace_load_no_iaddr", VG_(fnptr_to_fnentry)( trace_load_no_iaddr ),
                                argv );
   }

   if (guard)
      di->guard = guard;

   addStmtToIRSB( sb, IRStmt_Dirty(di) );
}

/* Add an ordinary read event, by adding a guarded read event with an
   always-true guard. */
static
void addEvent_Dr ( IRSB* sb, IRAtom* iaddr, IRAtom* daddr )
{
   addEvent_Dr_guarded(sb, iaddr, daddr, NULL);
}

static
IRSB* wc_instrument ( VgCallbackClosure* closure,
                      IRSB* sbIn,
                      const VexGuestLayout* layout, 
                      const VexGuestExtents* vge,
                      const VexArchInfo* archinfo_host,
                      IRType gWordTy, IRType hWordTy )
{
   /* Set up SB */
   IRSB* sbOut = deepCopyIRSBExceptStmts(sbIn);

   // Copy verbatim any IR preamble preceding the first IMark
   Int i = 0;
   while (i < sbIn->stmts_used && sbIn->stmts[i]->tag != Ist_IMark) {
      addStmtToIRSB( sbOut, sbIn->stmts[i] );
      i++;
   }

   IRAtom* iaddr = NULL;

   for (/*use current i*/; i < sbIn->stmts_used; i++) {
      IRStmt* st = sbIn->stmts[i];
      if (!st || st->tag == Ist_NoOp) continue;

      switch (st->tag) {
      case Ist_IMark: {
         iaddr = mkIRExpr_HWord( (HWord)st->Ist.IMark.addr );
         break;
      }

      case Ist_WrTmp: {
         IRExpr* data = st->Ist.WrTmp.data;
         if (data->tag == Iex_Load) {
            addEvent_Dr( sbOut, iaddr, data->Iex.Load.addr );
         }
         addStmtToIRSB( sbOut, st );
         break;
      }

      case Ist_LoadG: {
         IRLoadG* lg       = st->Ist.LoadG.details;
         IRType   type     = Ity_INVALID; /* loaded type */
         IRType   typeWide = Ity_INVALID; /* after implicit widening */
         typeOfIRLoadGOp(lg->cvt, &typeWide, &type);
         tl_assert(type != Ity_INVALID);
         addEvent_Dr_guarded( sbOut, iaddr, lg->addr,
                              lg->guard );
         addStmtToIRSB( sbOut, st );
         break;
      }

      case Ist_Dirty: {
         IRDirty* d = st->Ist.Dirty.details;
         if (d->mFx != Ifx_None) {
            // This dirty helper accesses memory.  Collect the details.
            tl_assert(d->mAddr != NULL);
            tl_assert(d->mSize != 0);
            if (d->mFx == Ifx_Read || d->mFx == Ifx_Modify)
               addEvent_Dr( sbOut, iaddr, d->mAddr );
         } else {
            tl_assert(d->mAddr == NULL);
            tl_assert(d->mSize == 0);
         }
         addStmtToIRSB( sbOut, st );
         break;
      }

      default:
         addStmtToIRSB( sbOut, st );
      }
   }

   return sbOut;
}

static void wc_fini (Int exitcode)
{
}

/* Just maybe we shouldn't be using this; it's not public API. */
Bool VG_(resolve_filename) ( Int fd, const HChar** result );

static Bool check_fd (int fd)
{
      const HChar* res;
      return (VG_(resolve_filename)(fd, &res) &&
              VG_(strncmp)(res, "/dev/dri/", 9) == 0);
}

static void pre_syscall ( ThreadId tid, UInt syscallno,
                          UWord* args, UInt nArgs )
{
}

static void post_syscall ( ThreadId tid, UInt syscallno,
                           UWord* args, UInt nArgs, SysRes res )
{
   if (sr_isError(res))
      return;

   /* TODO: Test the performance of reads to automatically decide
    * whether memory was mapped write-combine. */
   if (syscallno == __NR_mmap && check_fd(args[4])) {
      UWord addr = sr_Res(res);
      for (Word len = args[1]; len > 0; len -= P_PAGE_SIZE) {
         if (addr <= MAX_PRIMARY_ADDR)
            primary[addr >> P_PAGE_BITS] = 1;
         addr += P_PAGE_SIZE;
      }

      if (addr > MAX_PRIMARY_ADDR) {
         static int high_addr_warn = 0;
         if (high_addr_warn == 0) {
            VG_(printf)("Warning: BO with address %lx, above maximum for tracking\n", addr);
            high_addr_warn = 1;
         }
      }
   }

   if (syscallno == __NR_munmap) {
      UWord addr = args[0];
      for (Word len = args[1]; len > 0; len -= P_PAGE_SIZE) {
         if (addr < MAX_PRIMARY_ADDR)
            primary[addr >> P_PAGE_BITS] = 0;
         addr += P_PAGE_SIZE;
      }
   }
}

static Bool wc_eq_Error ( VgRes res, const Error* e1, const Error* e2 )
{
   return True;
}

static void wc_before_pp_Error ( const Error* err ) {
}

static void wc_pp_Error ( const Error* err )
{
   VG_(pp_ExeContext)( VG_(get_error_where)(err) );
}

static UInt wc_update_Error_extra( const Error* err )
{
   /* maybe this needs implementing? */
   return 0;
}

static Bool wc_is_recognised_suppression ( const HChar* name, Supp* su )
{
   if (VG_STREQ(name, "Read"))
      return True;
   return False;
}

static Bool wc_read_extra_suppression_info ( Int fd, HChar** bufpp,
                                             SizeT* nBufp, Int* lineno, Supp *su )
{
   return True;
}

static Bool wc_error_matches_suppression ( const Error* err, const Supp* su )
{
   return True;
}

static const HChar* wc_get_error_name ( const Error* err )
{
   return "Read";
}

static SizeT wc_get_extra_suppression_info ( const Error* err,
                                             /*OUT*/HChar* buf, Int nBuf )
{
   buf[0] = '\0';
   return 0;
}

static SizeT wc_print_extra_suppression_use ( const Supp *su,
                                              /*OUT*/HChar *buf, Int nBuf )
{
   return 0;
}

static void wc_update_extra_suppression_use ( const Error* err, const Supp* su)
{
   return;
}

static void wc_pre_clo_init (void)
{
   VG_(details_name)            ("ReadWC");
   VG_(details_version)         (NULL);
   VG_(details_description)     ("warn on reads from WC memory");
   VG_(details_copyright_author)(
      "Copyright (C) 2021, and GNU GPL'd, by Icecream95.");
   VG_(details_bug_reports_to)  (VG_BUGS_TO);

   VG_(details_avg_translation_sizeB) ( 275 );

   VG_(needs_syscall_wrapper)(pre_syscall, post_syscall);

   VG_(basic_tool_funcs)        (wc_post_clo_init,
                                 wc_instrument,
                                 wc_fini);

   VG_(needs_tool_errors)       (wc_eq_Error,
                                 wc_before_pp_Error,
                                 wc_pp_Error,
                                 True,/*show TIDs for errors*/
                                 wc_update_Error_extra,
                                 wc_is_recognised_suppression,
                                 wc_read_extra_suppression_info,
                                 wc_error_matches_suppression,
                                 wc_get_error_name,
                                 wc_get_extra_suppression_info,
                                 wc_print_extra_suppression_use,
                                 wc_update_extra_suppression_use);
   

   /* No needs, no core events to track */
}

VG_DETERMINE_INTERFACE_VERSION(wc_pre_clo_init)

/*--------------------------------------------------------------------*/
/*--- end                                                          ---*/
/*--------------------------------------------------------------------*/
